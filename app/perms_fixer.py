"""
This file is part of Plex Permissions Fixer, a simple application for setting
permissions on Plex library directories.
Copyright (C) 2022-2023 Luna Lucadou

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import json
import logging
import pathlib
import subprocess
from typing import Union, Tuple, Optional

import constants, gui_main, conf_mgr
#from app import gui, conf_mgr

# Setup logger
logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Setup logging to file
log_fh: logging.FileHandler = logging.FileHandler(constants.default_log_location)
log_fh.setLevel(logging.DEBUG)

# Setup log formatter
log_fmtr: logging.Formatter = logging.Formatter(constants.default_log_format)
log_fh.setFormatter(log_fmtr)

# Attach file logger
logger.addHandler(log_fh)
logger.debug("Logger setup complete")


def help_conf_file() -> None:
    """
    For CLI users, shows a sample config file and where it would be stored

    :return: None
    """
    logger.info('Displaying sample config file')
    print(constants.sample_conf_line_1)
    print(constants.sample_conf_line_2)
    print(constants.sample_conf_line_3)
    config: dict = {}
    with open(constants.sample_conf_location, 'r') as conf_file:
        config.update(json.load(conf_file))
    conf_file.close()
    print(json.dumps(config, indent=2))


def cli_fix_perms_bulk(config: conf_mgr.DirConf) -> None:
    """
    Given the validated configuration, sets permissions appropriately.

    For interactive CLI use, not GUI use.

    :param config: the configuration information
    :return: None
    """
    fallback_perms: str = config.get('default_perms')
    total_dirs: int = len(config.get('dirs'))
    current_dir: int = 1
    logger.info('Total directories to process: {n}'.format(
        n=str(total_dirs)
    ))
    print(constants.proc_dir_start.format(n=str(total_dirs)))
    for d in config.get('dirs'):
        target: str = d.get('dir')
        perms: Optional[str] = d.get('perms')
        if not perms:
            perms: str = fallback_perms

        logger.info('Processing entry {m}/{n}: {p} on {d}'.format(
            m=current_dir,
            n=total_dirs,
            p=perms,
            d=target
        ))
        print(constants.proc_dir_prog.format(
            m=current_dir,
            n=total_dirs,
            p=perms,
            d=target
        ))

        res: Tuple[bool, str] = fix_dir_perms(target, perms)
        if res[0]:
            print(constants.proc_dir_pass)
        else:
            print(constants.proc_dir_fail.format(err=res[1]))

        current_dir += 1
    logger.info('Processing complete!')
    print(constants.proc_dir_end.format(n=str(total_dirs)))


def fix_dir_perms(dir_path: Union[pathlib.Path, str], perms: str) -> Tuple[bool, str]:
    """
    Given a directory (type ``Path`` or ``str``), sets the permissions to the
    value given by ``perms`` (type ``str``, len 3-4 characters).

    If the operation succeeds, returns ``(True, '')``.
    Otherwise, returns ``(False, '[error message]')``.

    Possible failure conditions are as follows:

    * Invalid permissions (must be valid UNIX permissions)
    * Insufficient permissions for the current user for the specified directory
    * Invalid directory

    Note that you can use 3- or 4-digit permissions.
    For those unfamiliar with 4-digit permissions, a helpful explanation can be
    found at https://serverfault.com/a/344545.

    :param dir_path: the path to set permissions on
    :param perms: the Unix permissions to apply
    :return: whether it succeeded, and a string with more details
    """
    status: bool = True
    msg: str = ''
    # Escape dir_path
    # Because the path is contained in "" to ensure spaces don't cause errors,
    # I need to escape any ""s within the path
    target: str = str(dir_path).replace('"', '\\"')
    cmd: str = constants.perms_fix_cmd_tmpl.format(
        perms=perms,
        dir_path=target
    )

    if len(target) > 0:
        proc: Union[subprocess.CompletedProcess, subprocess.CompletedProcess[bytes]] = \
            subprocess.run(cmd, capture_output=True, shell=True)
        try:
            proc.check_returncode()
            logger.info('Succeeded setting permissions {p} on directory {d}'.format(
                p=perms,
                d=target
            ))
        except subprocess.CalledProcessError:
            status = False
            logger.error('Error executing the following command:')
            logger.error('\t{c}'.format(c=cmd))
            logger.error('\tStdout: "{s}"'.format(s=proc.stdout.decode()))
            logger.error('\tStderr: "{s}"'.format(s=proc.stderr.decode()))
            logger.error('\tExit code: {c}'.format(c=str(proc.returncode)))
            if len(proc.stderr.decode()) > 0:
                msg += proc.stderr.decode()
            else:
                msg += constants.cmd_error_unknown.format(code=str(proc.returncode))
    else:
        logger.info('Ignoring empty directory')
        msg += constants.cmd_error_blank

    return status, msg


if __name__ == '__main__':
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description=constants.help_application_desc
    )
    parser.add_argument(constants.flag_arg_conf_file, type=str,
                        help=constants.help_arg_conf_file,
                        dest=constants.name_arg_conf_file)
    parser.add_argument(constants.flag_arg_conf_help, action='store_true',
                        help=constants.help_arg_conf_help,
                        dest=constants.name_arg_conf_help)
    parser.add_argument(constants.flag_arg_gui, action='store_true',
                        help=constants.help_arg_gui,
                        dest=constants.name_arg_gui)
    args: argparse.Namespace = parser.parse_args()

    conf_path: Union[pathlib.Path, str] = constants.default_conf_location
    if type(eval('args.' + constants.name_arg_conf_file)) is str:
        # Is using eval() best practice?
        # No, but it's better than hoping nobody ever changes the names
        # of parameters (e.g. this gets translated into other languages).
        #
        # "is str" because "if ''" -> False but we need it to eval to True
        # because File > New calls "plex_fixer.py --gui --config-file ''"
        # and that needs to generate a blank/default config.
        # As such, we check the type():
        #   "plex_fixer.py --gui" -> NoneType
        #   "plex_fixer.py --gui --config-file ''" -> str
        # By checking the type(), we can easily detect blank config file
        # as opposed to none at all.
        conf_path: Union[pathlib.Path, str] = eval(
            'args.' + constants.name_arg_conf_file
        )

    if eval('args.' + constants.name_arg_gui):
        logger.info('Launching GUI...')
        gui_main.launch_gui(conf_path)
    elif eval('args.' + constants.name_arg_conf_help):
        # You're not supposed to use both --config-help and --gui at the same
        # time, so if the user does so anyway, I'll just ignore --config-help
        # since the GUI eliminates the need for manual config file creation
        help_conf_file()
    else:
        logger.info('Launching CLI...')
        conf_data: Tuple[Optional[conf_mgr.DirConf], Optional[str]] = \
            conf_mgr.read_conf(conf_path)
        if not conf_data[0]:  # No valid config data
            print(conf_data[1])
        else:  # Valid config data
            cli_fix_perms_bulk(conf_data[0])
