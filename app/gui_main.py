"""
This file is part of Plex Permissions Fixer, a simple application for setting
permissions on Plex library directories.
Copyright (C) 2022-2023 Luna Lucadou

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
import logging
import os.path
import pathlib
import re
import signal
import subprocess
import sys
import webbrowser
from typing import Optional, Tuple, Union, List

import PyQt6
from PyQt6 import QtCore, QtGui, QtWidgets
# The "from" line for importing PyQt6 stuff is solely for IDE completions
# and type hints; it is entirely unnecessary for the application's operation

import constants, conf_mgr, gui_about, gui_help, perms_fixer
#from app import constants, conf_mgr, gui_about, gui_help, perms_fixer
# For some reason using proper import paths doesn't want to work for GUI apps...
# TODO investigate why I cannot use proper namespaced imports

logger = logging.getLogger(__name__)


# Create window
class MainWindow(PyQt6.QtWidgets.QMainWindow):
    def __init__(self, app_conf_path: Union[pathlib.Path or str]):
        super().__init__()

        self.num_rows: int = 0
        self.startup_valid: bool = True
        self.setWindowTitle(constants.gui_window_title)
        self.gui_widget: PyQt6.QtWidgets.QWidget
        self.conf_path = app_conf_path

        # Get config data
        conf_data: Tuple[Optional[conf_mgr.DirConf], Optional[str]] = \
            conf_mgr.read_conf(self.conf_path)
        #conf_data: Tuple[Optional[conf_mgr.DirConf], Optional[str]] = \
        #    conf_mgr.read_conf(sys.argv[1])
        if not conf_data[0]:  # Invalid config data
            if conf_data[1] is not constants.conf_state_not_found:
                # Config data was found, but it was invalid/had an error
                # Need to tell user
                self.startup_valid = False
            # Fallback to a known-safe config
            conf_data: Tuple[Optional[conf_mgr.DirConf], Optional[str]] = \
                conf_mgr.get_fallback_conf(
                    blank_dirs=constants.gui_conf_fallback_blank_rows
                ), conf_data[1]
        self.default_perms: str = conf_data[0].get('default_perms')

        # Build basic layout
        gui_outer_layout: PyQt6.QtWidgets.QVBoxLayout = PyQt6.QtWidgets.QVBoxLayout()
        label_info: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(constants.gui_hdr_lbl_info)
        label_info.setWordWrap(True)
        gui_outer_layout.addWidget(label_info, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        gui_inner_layout_grid: PyQt6.QtWidgets.QGridLayout = PyQt6.QtWidgets.QGridLayout()
        gui_inner_layout_btm_row: PyQt6.QtWidgets.QBoxLayout = PyQt6.QtWidgets.QHBoxLayout()
        header_spacer: PyQt6.QtWidgets.QSpacerItem = PyQt6.QtWidgets.QSpacerItem(*[constants.gui_vert_spacer_height]*2)
        gui_outer_layout.addItem(header_spacer)

        # Populate table header
        header_label_paths: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(constants.gui_col_lbl_dirs)
        header_label_paths.setMinimumWidth(constants.gui_col_dirs_min_width)
        gui_inner_layout_grid.addWidget(header_label_paths, self.num_rows, 0, 1, 20, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        header_label_perms: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(constants.gui_col_lbl_perms)
        gui_inner_layout_grid.addWidget(header_label_perms, self.num_rows, 20, 1, 1, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        header_label_edit: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(constants.gui_col_lbl_edit)
        gui_inner_layout_grid.addWidget(header_label_edit, self.num_rows, 21, 1, 1, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        header_label_del: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(constants.gui_col_lbl_del)
        gui_inner_layout_grid.addWidget(header_label_del, self.num_rows, 22, 1, 1, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)

        # Populate bottom row
        footer_spacer: PyQt6.QtWidgets.QSpacerItem = PyQt6.QtWidgets.QSpacerItem(*[constants.gui_vert_spacer_height]*2)
        gui_inner_layout_btm_row.addItem(footer_spacer)
        footer_btn_add_row: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_btn_lbl_add)
        footer_btn_add_row.clicked.connect(
            lambda: self.increment_num_rows() and self.build_row_data(
                '',
                None,
                self.default_perms,
                gui_inner_layout_grid,
                self.get_num_rows()
            )
        )
        footer_btn_add_row.setToolTip(constants.gui_btn_lbl_add_tooltip)
        gui_inner_layout_btm_row.addWidget(footer_btn_add_row, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignLeft)
        footer_btn_run: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_btn_lbl_perms_apply)
        footer_btn_run.clicked.connect(
            lambda: self.run_button_clicked(
                self.collect_grid_row_data(gui_inner_layout_grid, self.default_perms)
            )
        )
        gui_inner_layout_btm_row.addWidget(footer_btn_run, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignRight)

        # Populate directory list
        # Don't need to check if conf_data[0] is present;
        # it's guaranteed to be
        for dir_entry in conf_data[0].get('dirs'):
            self.increment_num_rows()
            dir_path: str = dir_entry.get('dir')
            dir_perms: Optional[str] = dir_entry.get('perms')
            self.build_row_data(dir_path, dir_perms, self.default_perms,
                                gui_inner_layout_grid, self.num_rows)

        self.gui_widget = PyQt6.QtWidgets.QWidget()
        self.gui_widget.setLayout(gui_outer_layout)
        gui_outer_layout.addLayout(gui_inner_layout_grid)
        gui_outer_layout.addLayout(gui_inner_layout_btm_row)
        self.setCentralWidget(self.gui_widget)

        # Populate menu bar file menu
        #gui_menu_bar: PyQt6.QtWidgets.QMenuBar = PyQt6.QtWidgets.QMenuBar(self.gui_widget)
        gui_menu_bar: PyQt6.QtWidgets.QMenuBar = self.menuBar()
        gui_menu_file: PyQt6.QtWidgets.QMenu = gui_menu_bar.addMenu(constants.gui_menu_file_lbl)
        gui_menu_file_new: PyQt6.QtGui.QAction = gui_menu_file.addAction(constants.gui_menu_file_opt_new)
        gui_menu_file_new.setToolTip(constants.gui_menu_file_opt_new_tooltip)
        gui_menu_file_new.triggered.connect(
            lambda: self.launch_new_instance(None)
        )
        gui_menu_file_open: PyQt6.QtGui.QAction = gui_menu_file.addAction(constants.gui_menu_file_opt_open)
        gui_menu_file_open.setToolTip(constants.gui_menu_file_opt_open_tooltip)
        gui_menu_file_open.triggered.connect(
            lambda: self.launch_new_instance(self.open_conf_file())
        )
        gui_menu_file_save: PyQt6.QtGui.QAction = gui_menu_file.addAction(constants.gui_menu_file_opt_save)
        gui_menu_file_save.setToolTip(constants.gui_menu_file_opt_save_tooltip)
        gui_menu_file_save.triggered.connect(
            lambda: self.save_conf_file(
                self.collect_grid_row_data(gui_inner_layout_grid, self.default_perms),
                False
            )
        )
        gui_menu_file_save_as: PyQt6.QtGui.QAction = gui_menu_file.addAction(constants.gui_menu_file_opt_save_as)
        gui_menu_file_save_as.setToolTip(constants.gui_menu_file_opt_save_as_tooltip)
        gui_menu_file_save_as.triggered.connect(
            lambda: self.save_conf_file(
                self.collect_grid_row_data(gui_inner_layout_grid, self.default_perms),
                True
            )
        )
        gui_menu_file.addSeparator()
        gui_menu_file_settings: PyQt6.QtGui.QAction = gui_menu_file.addAction(constants.gui_menu_file_opt_settings)
        gui_menu_file_settings.triggered.connect(
            lambda: self.update_grid_default_perms(
                gui_inner_layout_grid,
                self.default_perms
            ) if self.settings_dialog() else None
        )
        gui_menu_file.addSeparator()
        gui_menu_file_quit: PyQt6.QtGui.QAction = gui_menu_file.addAction(constants.gui_menu_file_opt_quit)
        gui_menu_file_quit.setToolTip(constants.gui_menu_file_opt_quit_tooltip)
        gui_menu_file_quit.triggered.connect(self.quit_confirmation_dialog)
        # Populate menu bar help menu
        gui_menu_help: PyQt6.QtWidgets.QMenu = gui_menu_bar.addMenu(constants.gui_menu_help_lbl)
        gui_menu_help_help: PyQt6.QtGui.QAction = gui_menu_help.addAction(constants.gui_menu_help_opt_help)
        gui_menu_help_help.setToolTip(constants.gui_menu_help_opt_help_tooltip)
        gui_menu_help_help.triggered.connect(self.help_dialog)
        gui_menu_help_report: PyQt6.QtGui.QAction = gui_menu_help.addAction(constants.gui_menu_help_opt_report)
        gui_menu_help_report.triggered.connect(lambda x: webbrowser.open_new_tab(constants.application_url_bugs))
        gui_menu_help_feature: PyQt6.QtGui.QAction = gui_menu_help.addAction(constants.gui_menu_help_opt_request)
        gui_menu_help_feature.triggered.connect(lambda x: webbrowser.open_new_tab(constants.application_url_features))
        gui_menu_help.addSeparator()
        gui_menu_help_legal: PyQt6.QtGui.QAction = gui_menu_help.addAction(constants.gui_menu_help_opt_legal)
        gui_menu_help_legal.triggered.connect(self.license_dialog)
        gui_menu_help_about_qt: PyQt6.QtGui.QAction = gui_menu_help.addAction(constants.gui_menu_help_opt_about_qt)
        gui_menu_help_about_qt.triggered.connect(lambda: PyQt6.QtWidgets.QMessageBox.aboutQt(self))
        gui_menu_help_about_app: PyQt6.QtGui.QAction = gui_menu_help.addAction(constants.gui_menu_help_opt_about_app)
        gui_menu_help_about_app.triggered.connect(self.about_dialog)

        if not self.startup_valid:
            # Config was found, but it was invalid
            # Warn user it's started up with a fallback config
            conf_invalid_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
                parent=self.gui_widget
            )
            conf_invalid_dialog.setText(
                constants.gui_conf_invalid_help_main
            )
            conf_invalid_dialog.setInformativeText(
                constants.gui_conf_invalid_help_info
            )
            conf_invalid_dialog.setDetailedText(
                constants.gui_conf_invalid_help_detail.format(
                    err=conf_data[1]
                )
            )  # TODO this just says "Error: None" when it is displayed; give more informative message
            conf_invalid_dialog.setStandardButtons(
                PyQt6.QtWidgets.QMessageBox.StandardButton.Ok
            )
            conf_invalid_dialog.setIcon(
                PyQt6.QtWidgets.QMessageBox.Icon.Warning
            )
            conf_invalid_dialog.setWindowTitle(
                constants.gui_conf_invalid_title
            )
            conf_invalid_dialog.open()

    def closeEvent(self, event: PyQt6.QtCore.QEvent) -> None:
        """
        Intercepts users exiting (via X or ALT+F4) button and
        asks if they want to go back and save their configuration
        before exiting

        Overrides QWidget's closeEvent.

        :param event: the QCloseEvent
        :return: None
        """
        self.quit_confirmation_dialog()
        event.ignore()

    def get_num_rows(self) -> int:
        """
        Gets the number of rows that have been created thus far

        :return: the number of rows created
        """
        return self.num_rows

    def increment_num_rows(self) -> int:
        """
        Increments the number of rows that have been created thus far

        :return: the new number of rows created
        """
        self.num_rows += 1
        return self.num_rows

    def open_conf_file(self) -> str:
        """
        Opens a file picker and allows a user to pick a configuration file
        to use.

        Returns the path the user picked, if they picked any, or an empty str.

        :return: the selected path or ''
        """
        user_conf_path: Tuple[PyQt6.QtCore.QUrl, str] = PyQt6.QtWidgets.QFileDialog.getOpenFileName(
            self,
            constants.gui_open_window_title,
            str(self.conf_path),
            constants.gui_open_window_filter_full
        )

        return str(user_conf_path[0])

    def save_conf_file(self, gui_data: Optional[conf_mgr.DirConf],
                       save_as: bool) -> Tuple[bool, Optional[str]]:
        """
        Actions to be performed for File > Save or File > Save As:

        * Verify GUI table data is not None
            * If None, display an error window and exit
        * Verify save path length > 0
            * If false, set it to the default path
            * Then ignore save_as and open file picker
        * If save as is True, open file picker to specified path
        * Return if it succeeded and any error message

        :param gui_data: the current table data
        :param save_as: whether or not open the file picker
        :return: a tuple of if it succeeded and any error messages
        """
        res: Tuple[bool, Optional[str]]
        if gui_data:
            # Validate path
            if not self.conf_path or len(str(self.conf_path)) == 0:
                self.conf_path = constants.default_conf_location
                save_as = True

            # Get save path
            if save_as:
                user_conf_path: Tuple[PyQt6.QtCore.QUrl, str] = PyQt6.QtWidgets.QFileDialog.getSaveFileName(
                    self,
                    constants.gui_save_as_window_title,
                    str(self.conf_path),
                    constants.gui_save_as_window_filter_full
                )
                # getSaveFileName returns [QUrl, str], where:
                # - QUrl is the path
                # - str is the filter used
                #   - This is because you can use multiple filters and
                #     separate them by ";;", see:
                # https://www.riverbankcomputing.com/static/Docs/PyQt6/api/qtwidgets/qfiledialog.html#:~:text=If%20you%20want%20to%20use%20multiple%20filters%2C%20separate%20each%20one%20with%20two%20semicolons
                # As such, we only care about the QUrl
                self.conf_path = str(user_conf_path[0])
            # Ensure user selected a path (instead of hitting 'Cancel')
            if self.conf_path:
                res: Tuple[bool, Optional[str]] = conf_mgr.write_conf(gui_data, self.conf_path)

                if not res[0]:
                    # Error occurred - alert user
                    save_failed_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
                        parent=self.gui_widget
                    )
                    save_failed_dialog.setText(
                        constants.gui_save_as_error_window_help_main
                    )
                    save_failed_dialog.setInformativeText(
                        constants.gui_save_as_error_window_help_info.format(
                            file_path=self.conf_path
                        )
                    )
                    save_failed_dialog.setDetailedText(
                        res[1]
                        # If res[1] is None, the "Show details..." button does not appear;
                        # no need to worry about exceptions being thrown
                    )
                    save_failed_dialog.setStandardButtons(
                        PyQt6.QtWidgets.QMessageBox.StandardButton.Ok
                    )
                    save_failed_dialog.setIcon(
                        PyQt6.QtWidgets.QMessageBox.Icon.Critical
                    )
                    save_failed_dialog.setWindowTitle(
                        constants.gui_save_as_error_window_title
                    )
                    save_failed_dialog.open()
            else:
                res: Tuple[bool, Optional[str]] = (False, constants.gui_save_as_error_no_selection)
        else:
            self.run_invalid_state_dialog()
            res: Tuple[bool, Optional[str]] = (False, constants.gui_save_as_error_invalid_state)
        return res

    def run_button_clicked(self, gui_data: Optional[conf_mgr.DirConf]) -> None:
        """
        Actions to be performed when the Run button is clicked:

        * Verify GUI table data is not None
            * If None, display an error window and exit
        * Set permissions on each one
        * Display output in a new window

        :param gui_data: the current table data
        :return: None
        """
        if gui_data:
            dir_statuses: List[conf_mgr.PermsProgress] = self.run_perms_set_dialog(gui_data)
            self.run_perms_result_dialog(gui_data, dir_statuses)
        else:
            self.run_invalid_state_dialog()

    def run_perms_set_dialog(self, gui_data: conf_mgr.DirConf) \
            -> List[conf_mgr.PermsProgress]:
        """
        Sets permissions on the specified directory,
        with a progress bar to show the current state.

        :param gui_data: the current table data
        :return: the results of setting permissions on each directory
        """
        logger.info('Setting permissions on {0} directories...'.format(
            str(len(gui_data['dirs']))
        ))
        run_progress_dialog: PyQt6.QtWidgets.QProgressDialog = PyQt6.QtWidgets.QProgressDialog(
            parent=self.gui_widget
        )
        run_progress_dialog.setWindowTitle(
            constants.gui_run_window_title
        )
        run_progress_dialog.setMaximum(len(gui_data['dirs']))
        run_progress_dialog.open()

        dir_statuses: List[conf_mgr.PermsProgress] = []
        def_perms: str = gui_data['default_perms']
        for dir_obj in gui_data['dirs']:
            if run_progress_dialog.wasCanceled():
                cur_status: conf_mgr.PermsProgress = {
                    'dir': dir_obj,
                    'success': False,
                    'message': constants.gui_run_status_cancelled
                }
            else:
                # For each directory:
                # Send it to perms_fixer.fix_dir_perms
                # Check output - note which succeed and which fail
                # When done, close dialog box
                cur_perms: str = def_perms
                if dir_obj['perms'] and len(dir_obj['perms']) > 0:
                    cur_perms: str = dir_obj['perms']
                result: Tuple[bool, str] = perms_fixer.fix_dir_perms(
                    dir_obj['dir'],
                    cur_perms
                )
                cur_status: conf_mgr.PermsProgress = {
                    'dir': dir_obj,
                    'success': result[0],
                    'message': result[1]
                }
                dir_statuses.append(cur_status)

                run_progress_dialog.setValue(run_progress_dialog.value() + 1)
        run_progress_dialog.close()
        return dir_statuses

    def run_perms_result_dialog(self, gui_data: conf_mgr.DirConf,
                                results_data: List[conf_mgr.PermsProgress]) \
            -> None:
        """
        Given the GUI configuration state and permissions fixing results,
        displays a QMessageBox to the user with the number of successes,
        failures, and detailed information on the operation.

        :param gui_data: the current table data
        :param results_data: the results data from setting permissions
        :return: None
        """
        # Iterate over PermsProgress objects, total # of errors
        # Open a new QMessageBox
        # Set icon based on # of errors (0 = info, >1 = warning)
        # List out every success and failure in the detailed message box
        logger.info('Displaying results from setting permissions on {0} directories...'.format(
            str(len(gui_data['dirs']))
        ))

        # Total errors
        errors: int = 0
        for res in results_data:
            if not res['success']:
                errors += 1

        detailed_text: str = ''
        # Prepare detailed text
        if errors == 0:
            detailed_text += constants.gui_run_status_window_help_detail_success
        else:
            # Format:
            # * <dir> - <message>
            detailed_text += constants.gui_run_status_window_help_detail_failure
            for res in results_data:
                detailed_text += '{0} {1} - {2}'.format(
                    constants.gui_run_status_window_help_detail_bullet,
                    res['dir']['dir'],
                    res['message']
                )

        run_status_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
            parent=self.gui_widget
        )
        run_status_dialog.setText(
            constants.gui_run_status_window_help_main
        )
        run_status_dialog.setInformativeText(
            constants.gui_run_status_window_help_info.format(
                m=str(len(results_data) - errors),
                n=str(len(results_data))
            )
        )
        run_status_dialog.setDetailedText(
            detailed_text
        )
        run_status_dialog.setStandardButtons(
            PyQt6.QtWidgets.QMessageBox.StandardButton.Ok
        )
        if errors == 0:
            run_status_dialog.setIcon(
                PyQt6.QtWidgets.QMessageBox.Icon.Information
            )
        else:
            run_status_dialog.setIcon(
                PyQt6.QtWidgets.QMessageBox.Icon.Warning
            )
        run_status_dialog.setWindowTitle(
            constants.gui_run_status_window_title
        )
        if errors != 0:
            run_status_dialog.setStyleSheet(constants.gui_css_for_small_windows)
            # To ensure the window is wide enough for the file paths in the
            # DetailedText.
            # See the comment on the variable in constants.py for info on
            # why this is needed.

        run_status_dialog.open()

    def run_invalid_state_dialog(self) -> None:
        """
        Shows an error message to the user that the application was unable
        to read its state data from the table.

        :return: None
        """
        logger.warning('Run or save as button clicked but no valid data is present')
        conf_invalid_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
            parent=self.gui_widget
        )
        conf_invalid_dialog.setText(
            constants.gui_conf_invalid_help2_main
        )
        conf_invalid_dialog.setInformativeText(
            constants.gui_conf_invalid_help2_info
        )
        conf_invalid_dialog.setDetailedText(
            constants.gui_conf_invalid_help2_detail
        )
        conf_invalid_dialog.setStandardButtons(
            PyQt6.QtWidgets.QMessageBox.StandardButton.Ok
        )
        conf_invalid_dialog.setIcon(
            PyQt6.QtWidgets.QMessageBox.Icon.Critical
        )
        conf_invalid_dialog.setWindowTitle(
            constants.gui_conf_invalid_title
        )
        conf_invalid_dialog.open()

    def help_dialog(self) -> None:
        """
        Shows help information for the application to the user.

        :return: None
        """
        self.help_window = gui_help.HelpWindow()
        self.help_window.show()
        # TODO move into lambda in __init__ instead of dedicated function?

    def license_dialog(self) -> None:
        """
        Shows the application license to the user.

        :return: None
        """
        license_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
            parent=self.gui_widget
        )
        license_dialog.setText(
            constants.gui_license_window_help_main
        )
        license_dialog.setInformativeText(
            constants.gui_license_window_help_info
        )
        license_dialog.setDetailedText(
            constants.gui_license_window_help_detail
        )
        license_dialog.setStandardButtons(
            PyQt6.QtWidgets.QMessageBox.StandardButton.Ok
        )
        license_dialog.setIcon(
            PyQt6.QtWidgets.QMessageBox.Icon.Information
        )
        license_dialog.setWindowTitle(
            constants.gui_license_window_help_title
        )

        license_dialog.show()

    def about_dialog(self) -> None:
        """
        Shows information about the application to the user.

        :return: None
        """
        self.about_window = gui_about.AboutWindow()
        self.about_window.show()
        # TODO move into lambda in __init__ instead of dedicated function?

    def settings_dialog(self) -> bool:
        perms_valid: bool = True
        res: Tuple[str, bool] = PyQt6.QtWidgets.QInputDialog.getText(
            self,  # Parent QWidget
            constants.gui_settings_title,  # Title bar text
            constants.gui_settings_opt_perms_desc,  # Label text
            PyQt6.QtWidgets.QLineEdit.EchoMode.Normal,  # Input mode
            self.default_perms  # Default text
        )
        # If the user presses Ok:
        #   res[0] = 'what was in the text box'
        #   res[1] = True
        # If the user presses Cancel:
        #   res[0] = '' (empty str)
        #   res[1] = False
        if res[1] and re.search(constants.gui_settings_opt_perms_regex, res[0]):
            res_search: re.Match = re.search(constants.gui_settings_opt_perms_regex, res[0])
            new_perms: str = res_search.group(0)
            self.default_perms = new_perms
        elif res[1]:
            perms_valid = False
            # Display error message
            validation_failure_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
                parent=self.gui_widget
            )
            validation_failure_dialog.setText(
                constants.gui_conf_invalid_help3_main
            )
            validation_failure_dialog.setInformativeText(
                constants.gui_conf_invalid_help3_info
            )
            validation_failure_dialog.setDetailedText(
                constants.gui_conf_invalid_help3_detail
            )
            validation_failure_dialog.setStandardButtons(
                PyQt6.QtWidgets.QMessageBox.StandardButton.Ok
            )
            validation_failure_dialog.setIcon(
                PyQt6.QtWidgets.QMessageBox.Icon.Warning
            )
            validation_failure_dialog.setWindowTitle(
                constants.gui_conf_invalid_title
            )

            validation_failure_dialog.show()
        return perms_valid

    def quit_confirmation_dialog(self) -> None:
        """
        Confirms that the user is sure they want to exit.

        :return: None
        """
        quit_dialog: PyQt6.QtWidgets.QMessageBox = PyQt6.QtWidgets.QMessageBox(
            parent=self.gui_widget
        )
        quit_dialog.setText(
            constants.gui_quit_confirm_help_main
        )
        quit_dialog.setInformativeText(
            constants.gui_quit_confirm_help_info
        )
        quit_dialog.setStandardButtons(
            PyQt6.QtWidgets.QMessageBox.StandardButton.Yes |
            PyQt6.QtWidgets.QMessageBox.StandardButton.No
        )
        quit_dialog.setDefaultButton(
            PyQt6.QtWidgets.QMessageBox.StandardButton.Yes
        )
        quit_dialog.setIcon(
            PyQt6.QtWidgets.QMessageBox.Icon.Question
        )
        quit_dialog.setWindowTitle(
            constants.gui_quit_confirm_title
        )

        choice: int = quit_dialog.exec()
        if choice == PyQt6.QtWidgets.QMessageBox.StandardButton.Yes:
            sys.exit()

    @staticmethod
    def build_row_data(dir_path: str, perms: Optional[str], default_perms: str,
                       containing_grid: PyQt6.QtWidgets.QGridLayout,
                       row_num: int) -> \
            Tuple[PyQt6.QtWidgets.QLineEdit, PyQt6.QtWidgets.QLineEdit,
                  PyQt6.QtWidgets.QPushButton, PyQt6.QtWidgets.QPushButton]:
        """
        Given a directory, the directory grid, row number, and at least one of
        permissions or default permissions, creates a series of Qt widgets to
        be inserted into the GUI directory grid.

        :param dir_path: the directory to target
        :param perms: the permissions to use for it (optional)
        :param default_perms: the default permissions to fall back on if ``perms`` is not present
        :param containing_grid: the grid the widgets will be placed onto
        :param row_num: the row in the grid this will be
        :return: a Tuple containing four widgets
        """
        # Maintainer's Note:
        #
        # Currently, the widgets do not have Parent widgets specified.
        # If this changes, dir_grid_row_rm method will need to be updated
        # (see comment down there for more info).
        dir_line_field: PyQt6.QtWidgets.QLineEdit = PyQt6.QtWidgets.QLineEdit(dir_path)
        dir_line_field.setMinimumWidth(constants.gui_col_dirs_min_width)
        containing_grid.addWidget(dir_line_field, row_num, 0, 1, 20, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        dir_perms_field: PyQt6.QtWidgets.QLineEdit = PyQt6.QtWidgets.QLineEdit(perms)
        dir_perms_field.setPlaceholderText(default_perms)
        containing_grid.addWidget(dir_perms_field, row_num, 20, 1, 1, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        dir_pick_btn: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_btn_lbl_edit)
        dir_pick_btn.clicked.connect(
            lambda: MainWindow.pick_new_dir(dir_line_field)
        )
        containing_grid.addWidget(dir_pick_btn, row_num, 21, 1, 1, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        dir_del_btn: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_btn_lbl_del)
        dir_del_btn.clicked.connect(
            lambda: MainWindow.dir_grid_row_rm(
                dir_line_field,
                dir_perms_field,
                dir_pick_btn,
                dir_del_btn,
                containing_grid
            )
        )
        containing_grid.addWidget(dir_del_btn, row_num, 22, 1, 1, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignTop)
        return dir_line_field, dir_perms_field, dir_pick_btn, dir_del_btn

    @staticmethod
    def pick_new_dir(line_to_update: PyQt6.QtWidgets.QLineEdit) -> None:
        """
        Given a text box, launches a file picker window so the user can
        navigate to a new directory to list in the text box

        :param line_to_update: the text box containing the directory to replace
        :return: None
        """
        existing_path: str = line_to_update.text()
        picker: PyQt6.QtWidgets.QFileDialog = PyQt6.QtWidgets.QFileDialog()
        picker.setFileMode(PyQt6.QtWidgets.QFileDialog.FileMode.Directory)
        picker.setDirectory(existing_path)
        if picker.exec():
            line_to_update.setText(picker.selectedFiles()[0])

    @staticmethod
    def dir_grid_row_rm(dir_field: PyQt6.QtWidgets.QLineEdit,
                        perms_field: PyQt6.QtWidgets.QLineEdit,
                        change_btn: PyQt6.QtWidgets.QPushButton,
                        del_btn: PyQt6.QtWidgets.QPushButton,
                        containing_grid: PyQt6.QtWidgets.QGridLayout) -> None:
        """
        Removes a row from the list of directories to fix permissions on

        :param dir_field: the text box with the directory in question
        :param perms_field: the text box with the permissions of the directory
        :param change_btn: the button to change the directory
        :param del_btn: the button to remove the text box and change button
        :param containing_grid: the grid containing all three
        :return: None
        """
        # Maintainer's Note:
        #
        # Don't need to call .setParent(None) on each widget
        # because parent widget was never set on any of them to begin with.
        # If, in the future, the parent widget is specified for all of them
        # this code will need to be adjusted accordingly.
        containing_grid.removeWidget(dir_field)
        dir_field.hide()
        containing_grid.removeWidget(perms_field)
        perms_field.hide()
        containing_grid.removeWidget(change_btn)
        change_btn.hide()
        containing_grid.removeWidget(del_btn)
        del_btn.hide()

    @staticmethod
    def collect_grid_row_data(gui_inner_layout: PyQt6.QtWidgets.QGridLayout,
                              default_perms: str) -> Optional[conf_mgr.DirConf]:
        """
        Given an inner layout and the default permissions, builds a
        DirConf object with all data in the table.

        If any errors are encountered, returns None.

        :param gui_inner_layout: the QGridLayout containing everything
        :param default_perms: the default directory permissions
        :return: all the directories listed and their permissions or None
        """
        logger.info('Collecting current configuration data...')
        col_offset_dir: int = 0
        col_offset_perms: int = 1

        grid_index_start: int = 4
        # 0 = "Directory" column header
        # 1 = "Permissions" column header
        # 2 = "Change Directory" column header
        # 3 = "Remove" column header
        # 4 = first Directory text field
        # 5 = first Permissions text field
        # 6 = first Change Directory button
        # 7 = first Remove button
        # etc.
        grid_index_end: int = gui_inner_layout.count()

        dir_entries: List[conf_mgr.DirEntry] = []
        errors: int = 0
        for grid_index in range(grid_index_start, grid_index_end, 4):
            col_index_dir: int = grid_index + col_offset_dir
            col_index_perms: int = grid_index + col_offset_perms
            cur_dir_obj: PyQt6.QtWidgets.QWidget = gui_inner_layout.itemAt(col_index_dir).widget()
            cur_perms_obj: PyQt6.QtWidgets.QWidget = gui_inner_layout.itemAt(col_index_perms).widget()
            # itemAt: https://stackoverflow.com/a/45858478
            cur_dir_path: str = ''
            cur_dir_perms: str = ''
            if type(cur_dir_obj) is PyQt6.QtWidgets.QLineEdit:
                cur_dir_path += cur_dir_obj.text()
            else:
                errors += 1
                logger.error(
                    'Expected index={0} to be directory QLineEdit, got {1}'.format(
                        str(col_index_dir),
                        str(type(cur_dir_obj))
                    )
                )
            if type(cur_perms_obj) is PyQt6.QtWidgets.QLineEdit:
                cur_dir_perms += cur_perms_obj.text()
            else:
                errors += 1
                logger.error(
                    'Expected index={0} to be permissions QLineEdit, got {1}'.format(
                        str(col_index_perms),
                        str(type(cur_perms_obj))
                    )
                )

            if len(cur_dir_path) > 0:
                cur_dir_data: conf_mgr.DirEntry = {
                    'dir': cur_dir_path,
                    'perms': cur_dir_perms
                }
                dir_entries.append(cur_dir_data)
            else:
                logger.info('Skipping row containing index {0} (empty directory cell)...'.format(
                    str(grid_index)
                ))

        collected_data: conf_mgr.DirConf = {
            'default_perms': default_perms,
            'dirs': dir_entries
        }

        if errors == 0:
            return collected_data
        else:
            return None

    @staticmethod
    def update_grid_default_perms(gui_inner_layout: PyQt6.QtWidgets.QGridLayout,
                                  new_default_perms: str) -> None:
        """
        Given an inner layout and the default permissions, updates the
        default permissions placeholder text.

        :param gui_inner_layout: the QGridLayout containing everything
        :param new_default_perms: the updated default directory permissions
        :return: all the directories listed and their permissions or None
        """
        logger.info('Updating default permissions...')
        col_offset_perms: int = 1

        grid_index_start: int = 4
        # 0 = "Directory" column header
        # 1 = "Permissions" column header
        # 2 = "Change Directory" column header
        # 3 = "Remove" column header
        # 4 = first Directory text field
        # 5 = first Permissions text field
        # 6 = first Change Directory button
        # 7 = first Remove button
        # etc.
        grid_index_end: int = gui_inner_layout.count()

        errors: int = 0
        for grid_index in range(grid_index_start, grid_index_end, 4):
            col_index_perms: int = grid_index + col_offset_perms
            cur_perms_obj: PyQt6.QtWidgets.QWidget = gui_inner_layout.itemAt(col_index_perms).widget()
            # itemAt: https://stackoverflow.com/a/45858478

            if type(cur_perms_obj) is PyQt6.QtWidgets.QLineEdit:
                cur_perms_obj.setPlaceholderText(new_default_perms)
            else:
                errors += 1
                logger.error(
                    'Expected index={0} to be permissions QLineEdit, got {1}'.format(
                        str(col_index_perms),
                        str(type(cur_perms_obj))
                    )
                )

    @staticmethod
    def launch_new_instance(conf_path: Optional[Union[pathlib.Path, str]]) -> None:
        """
        Launches a new instance of the application, given a configuration
        file path.

        * Valid path -> launch new instance with that path
        * Empty path -> cancel
        * NoneType or invalid path -> launch new instance with blank configuration

        :param conf_path: the configuration file to use or None
        :return: None
        """
        launch_args: List[str] = [os.path.basename(sys.executable)] + sys.argv
        # sys.argv[0] is always the script
        # sys.executable is the Python binary

        if len(launch_args) > 2 and launch_args[2].startswith('--'):
            # ['python', '/path/to/script.py', '--gui', '--config-file', '/path/to/config.json']
            # Only want first 2 elements; don't want to risk two --config-file params
            launch_args = launch_args[:2]

        # Append GUI flag
        launch_args.append(constants.flag_arg_gui)

        if (conf_path and len(str(conf_path)) > 0) or (conf_path is None):
            # Have to explicitly check if None because
            # "elif not conf_path" -> True when conf_path is '' (empty str)

            launch_args.append(constants.flag_arg_conf_file)
            if conf_path and len(str(conf_path)) > 0:
                # Specify config file
                launch_args.append(str(conf_path))
            else:
                launch_args.append('""')

            # Prevent zombie processes by announcing intent to ignore the
            # child process status
            signal.signal(signal.SIGCHLD, signal.SIG_IGN)
            # Note that signal.SIGCHLD is UNIX-exclusive
            # https://docs.python.org/3/library/signal.html#signal.SIGCHLD

            # Launch child process
            subprocess.Popen(launch_args, close_fds=True)
            # close_fds=True -> detached process, meaning the parent process
            # can exit without waiting on child
            # https://stackoverflow.com/a/34459371
            # I prefer subprocess.run but Popen appears to be the only
            # option for spawning background processes.
            # This does preserve the environment, so no need to worry about
            # re-entering pipenv or anything.


def launch_gui(conf_path: Union[pathlib.Path or str]):
    #sys.argv += [conf_path]
    gui_app: PyQt6.QtWidgets.QApplication = PyQt6.QtWidgets.QApplication(sys.argv)

    gui_window: PyQt6.QtWidgets.QMainWindow = MainWindow(conf_path)
    gui_window.show()

    # Start event loop
    gui_app.exec()

