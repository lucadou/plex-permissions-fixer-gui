"""
This file is part of Plex Permissions Fixer, a simple application for setting
permissions on Plex library directories.
Copyright (C) 2022-2023 Luna Lucadou

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
import webbrowser

import PyQt6
from PyQt6 import QtCore, QtGui, QtWidgets

import constants
#from app import constants


class AboutWindow(PyQt6.QtWidgets.QWidget):
    """
    I need more than a regular QMessageBox for the About window,
    so it's easier to just create another class for it.
    """

    def __init__(self):
        super().__init__()
        about_layout: PyQt6.QtWidgets.QLayout = PyQt6.QtWidgets.QVBoxLayout()
        self.setWindowTitle(constants.gui_about_window_title)

        # About text
        about_label_desc: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(
            constants.gui_prog_description
        )
        about_label_desc.setWordWrap(True)
        about_layout.addWidget(about_label_desc)
        about_label_copyright: PyQt6.QtWidgets.QLabel = PyQt6.QtWidgets.QLabel(
            constants.application_copyright_blurb
        )
        about_label_copyright.setWordWrap(True)
        about_layout.addWidget(about_label_copyright)

        # Populate middle button row
        about_btn_row: PyQt6.QtWidgets.QBoxLayout = PyQt6.QtWidgets.QHBoxLayout()
        top_spacer: PyQt6.QtWidgets.QSpacerItem = PyQt6.QtWidgets.QSpacerItem(*[constants.gui_vert_spacer_height]*2)
        about_btn_row.addItem(top_spacer)
        about_btn_row_credits: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_about_window_credits_lbl)
        about_btn_row_credits.setToolTip(constants.gui_about_window_credits_lbl_tooltip)
        about_btn_row_credits.clicked.connect(
            lambda: webbrowser.open_new_tab(constants.application_url_credits)
        )
        about_btn_row.addWidget(about_btn_row_credits, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignCenter)
        about_btn_row_website: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_about_window_website_lbl)
        about_btn_row_website.setToolTip(constants.gui_about_window_website_lbl_tooltip)
        about_btn_row_website.clicked.connect(
            lambda: webbrowser.open_new_tab(constants.application_url_homepage)
        )
        about_btn_row.addWidget(about_btn_row_website, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignCenter)
        about_btn_row_changelog: PyQt6.QtWidgets.QPushButton = PyQt6.QtWidgets.QPushButton(constants.gui_about_window_changelog_lbl)
        about_btn_row_changelog.setToolTip(constants.gui_about_window_changelog_lbl_tooltip)
        about_btn_row_changelog.clicked.connect(
            lambda: webbrowser.open_new_tab(constants.application_url_changelog)
        )
        about_btn_row.addWidget(about_btn_row_changelog, alignment=PyQt6.QtCore.Qt.AlignmentFlag.AlignCenter)
        about_layout.addLayout(about_btn_row)

        self.setLayout(about_layout)
