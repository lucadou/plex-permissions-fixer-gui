"""
This file is part of Plex Permissions Fixer, a simple application for setting
permissions on Plex library directories.
Copyright (C) 2022-2023 Luna Lucadou

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
import PyQt6
from PyQt6 import QtCore, QtGui, QtWidgets

import constants
#from app import constants


class HelpWindow(PyQt6.QtWidgets.QWidget):
    """
    I need more than a regular QMessageBox for the Help window,
    so it's easier to just create another class for it.
    """

    def __init__(self):
        super().__init__()
        self.setWindowTitle(constants.gui_help_window_title)
        self.setMinimumSize(
            constants.gui_hlp_menu_min_width,
            constants.gui_hlp_menu_min_height
        )
        help_layout: PyQt6.QtWidgets.QLayout = PyQt6.QtWidgets.QVBoxLayout()

        # Tab Bar
        help_tab_bar: PyQt6.QtWidgets.QTabWidget = PyQt6.QtWidgets.QTabWidget(self)
        help_layout.addWidget(help_tab_bar)

        # Markdown Objects
        help_md_tab_general: PyQt6.QtWidgets.QTextBrowser = PyQt6.QtWidgets.QTextBrowser(None)
        # Parent QWidget set to None instead of self because:
        #   2. Create a QWidget for each of the pages in the tab dialog,
        #   but do not specify parent widgets for them.
        # Source: https://www.riverbankcomputing.com/static/Docs/PyQt6/api/qtwidgets/qtabwidget.html
        help_md_tab_general.setSource(
            PyQt6.QtCore.QUrl('file:docs/general.en.md'),
            PyQt6.QtGui.QTextDocument.ResourceType.MarkdownResource
        )
        help_md_tab_usage: PyQt6.QtWidgets.QTextBrowser = PyQt6.QtWidgets.QTextBrowser(None)
        help_md_tab_usage.setSource(
            PyQt6.QtCore.QUrl('file:docs/usage.en.md'),
            PyQt6.QtGui.QTextDocument.ResourceType.MarkdownResource
        )
        help_md_tab_perms: PyQt6.QtWidgets.QTextBrowser = PyQt6.QtWidgets.QTextBrowser(None)
        help_md_tab_perms.setSource(
            PyQt6.QtCore.QUrl('file:docs/unix_perms.en.md'),
            PyQt6.QtGui.QTextDocument.ResourceType.MarkdownResource
        )

        # Add Markdown to Tabs
        help_tab_bar.addTab(help_md_tab_general, constants.gui_help_window_tab_general)
        help_tab_bar.addTab(help_md_tab_usage, constants.gui_help_window_tab_usage)
        help_tab_bar.addTab(help_md_tab_perms, constants.gui_help_window_tab_perms)

        self.setLayout(help_layout)

