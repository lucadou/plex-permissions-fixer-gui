## UNIX Permissions

UNIX permissions are a set of nine bits, divided into three octets,
which determine who can access a given file or directory
and how they can access it on a UNIX system.

Each octet (group of three bits) is represented in its decimal (base 10)
equivalent.
To explain how this works, you must first understand binary (base 2).
If you are not already familiar with binary, please scroll down to the
"Base 2 (Binary)" section for an explanation on the subject.

The organization of each octet is as follows:

* Third bit - Read (read a file or list a directory's contents)
* Second bit - Write (write to a file or create/delete files in a directory)
* First bit - eXecute (run a program or enter a directory)

As such, a file which you can Read, Write, and eXecute would have at least
one octet set to `111` (`1 * 2^2 + 1 * 2^1 + 1 * 2^0 = 7(10)`).

Each octet is then multiplied by 10 to the power of its position:

* Third position - owner User
* Second position - owner Group
  * Note that the owner Group is not necessarily related to the owner User -
    you can set an owner Group which the owner User is not a member of
  * All that matters is that anyone who attempts to access it
    (besides the owner User) is a member of the owner Group
* First position - Others (neither U nor G)

So the permissions `111110101` would be displayed as:

```
User     Group    Other
1 1 1    1 1 0    1 0 1
R W X    R W      R   X
4+2+1    4+2+0    4+0+1
7        6        5
* 10^2   * 10^1   * 10^0
700    + 60     + 5
= 765
```

With this, you can craft appropriate permissions for any directory you wish.

However, depending on how your Linux install is set up, you can experience
odd behavior, especially for Plex library folders on alternate disks,
and even if you set the permissions correctly, adding new files to your
libraries may require re-chmod-ing your library folders to reset any
permissions retained from the previous location.

And that is where this application comes in: simply add your Plex library
folders, set the appropriate permissions strings, and let this application
handle the hard work for you.

This application defaults to using `755` (U+RWX, GO+RX) as recommended in the
Plex Linux Permissions Guide (linked below), but you can easily override this
behavior.

Sources used for this section:

* https://mason.gmu.edu/~montecin/UNIXpermiss.htm
* https://support.plex.tv/articles/200288596-linux-permissions-guide/

### Base 2 (Binary)

Stock imagery of computer-related topics frequently features 1s and 0s.
But while many people have associated sequences of 1s and 0s with "complex
computer-y stuff", few know the significance of these numbers.

In short, it is an abstraction for arbitrary voltage levels.
Although binary is oftentimes billed as "0 = off, 1 = on", in reality,
computer circuits deal with high and low voltage, not high and no voltage.
The reason for this is simple:
What would the difference between an uninitialized circuit and 0 be
if 0 *actually* meant "off" (i.e. 0 volts)?

It may seem a silly question, but it is actually foundational to computing
since a computer cannot start up if it does not know what has been initialized
and what has not.

So when a computer interprets something as 1 or 0, it's actually thinking
about high vs low voltage.
There is no one specific voltage that represents 1 or 0 in computing, there
are only internal voltages and external voltages (external voltages typically
being defined by formal specifications to ensure cross-compatibility).

The voltage of a 1 vs 0 may seem an irrelevant tangent, but the important
thing to understand is that these are merely an abstraction to make things
easier for computer engineers and programmers, and that is key to understanding
base 2 (and every number system, for that matter).

When you see a number in base 10 (the number system humans have largely agreed
upon today), you may think "that's just a number", but it's also an abstraction.

Take the base 10 number 255(10).
(The (10) indicates the base; normally, you would use subscripts to indicate
the base but the formatting system these help files use does not have the
ability to do subscripts.)

Technically, 255 does not exist as a formally defined base 10 number.
Rather, it's the combination of other numbers:

255(10) = 2 \* 10^2 + 5 \* 10^1 + 5 \* 10^0 = 200 + 50 + 5

Base 10 means there are 10 possible unique representations (0-9) and so each
"place" in a number is merely that multiplied by 10 to the power of its position.

Likewise, if you wanted to represent 255 in base 2...

255(10) = 1 \* 2^7 + 1 \* 2^6 + 1 \* 2^5 + 1 \* 2^4 + 1 \* 2^3 + 1 \* 2^2 + 1 \* 2^1 + 1 \* 2^0 =
128 + 64 + 32 + 16 + 8 + 4 + 2 + 1 = 11111111(2)

So if you wanted to convert a base 10 number (e.g. 110) into base 2,
this is the process:

1. Locate the nearest base 10 number that is a multiple of 2 but less than or equal to
   the number (e.g. 2^7 is 128, but that is larger than 110, so we use 2^6,
   which is 64)
2. Subtract that number out (110 - 64 = 46)
3. Locate the next multiple of 2 that is less than or equal to 46 (in this case, 2^5, or 32)
4. Subtract that number out (46 - 32 = 14)
5. Locate the next multiple of 2 that is less than or equal to 14 (in this case, 2^3, or 8)
6. Subtract that number out (14 - 8 = 6)
7. Locate the next multiple of 2 that is less than or equal to 6 (in this case, 2^2, or 4)
8. Subtract that number out (6 - 4 = 2)
9. Locate the next multiple of 2 that is less than or equal to 2 (in this case, 2^1, or 2)
10. Subtract that number out (2 - 2 = 0)
11. We have now reached 0, so we are complete

Using this method, we have found that:

```
110(10) = 1 * 2^6 + 1 * 2^5 + 0 * 2^4 + 1 * 2^3 + 1 * 2^2 + 1 * 2^1 + 0 * 2^0
        = 1 * 64  + 1 * 32  + 0 * 16  + 1 * 8   + 1 * 4   + 1 * 2   + 0 * 1
        = 1         1         0         1         1         1         0
        = 1101110(2)
```

And reversing the equations, we find:

```
1101110(2) = 1 * 2^6 + 1 * 2^5 + 0 * 2^4 + 1 * 2^3 + 1 * 2^2 + 1 * 2^1 + 0 * 2^0
           = 1 * 64  + 1 * 32  + 0 * 16  + 1 * 8   + 1 * 4   + 1 * 2   + 0 * 1
           = 64      + 32      + 0       + 8       + 4       + 2       + 0
           = 110(10)
```

You can make minor adjustments to this system to convert into any arbitrary base.

### Other Number Bases

Alternate numbering systems may seem weird at first, but in reality it is just
*thinking* about dealing with alternate bases that throws most people off.

When presented to us as "this is how this is", we tend not to notice as much,
as shown by how well we handle non-base 10 numbers all the time in other areas
of life:

* Minutes and seconds are measured in base 60 (00-59)
* Hours are measured in base 12+2 (01-12 AM/PM) or 24 (00-23),
  depending on where you live/work
* Days are measured in:
  * Base 7 (days of the week)
  * Base 28/29/30/31 (days in a month on the Gregorian calendar)
  * Base 365/366 (days in a year on the Gregorian calendar)
* Months are measured in base 12 (on the Gregorian calendar)
* Financial reporting operates in base 4 (quarters in a fiscal year)
* Western musical letter notation operates in base 7
* The metric system uses base 10
* Imperial units use a nightmarish mix of bases
* Programmers frequently use hexadecimal (base 16, represented as 0-9 + A-F)
  as a useful abstraction over raw binary

Number systems may seem complex, but they are actually quite simple.
It is just abstractions all the way down.

And if you are wondering "so how does a series of 1s and 0s translate to
a letter on my screen?," we have
character encodings (https://en.wikipedia.org/wiki/Character_encoding)
to thank for that -
the letter "A" may be "short long" (or, to use base 2, "01") in Morse code,
but it is 65(10), 0x41(16), or 1000001(2) in
ASCII (https://en.wikipedia.org/wiki/ASCII#Printable_characters).

It's an entirely arbitrary abstraction, but it's a standard that was widely
adopted because every company having their own encoding would have been
foolish at best.
