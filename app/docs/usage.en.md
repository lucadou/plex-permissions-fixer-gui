## Usage

This application is quite simple.
Each row contains a directory
(which you can set by pressing the "..." button in the "Change Directory" column
or typing it in manually)
and a permissions value
(which can be set by typing it in to the appropriate box in the
"Permissions" column).

To add a new row, press the "+" button at the bottom left of the main window.

To remove an existing row, press the "X" button in the last column of the
appropriate row.

When you are ready to set the specified permissions on the listed directories,
press the "Fix Permissions" button at the bottom right of the main window.
A new window will appear to show the application's progress.

(For help with the Permissions column, see the "About UNIX Permissions" tab.)

### The File Menu

In the "File" menu, there are several options:

* New
  * This launches a new instance of the application with no directories or
    permissions set.
* Open
  * This launches a new instance of the application with a specified
    configuration file pre-loaded.
* Save
  * Saves the current configuration to the path it was opened with.
  * If it was opened without specifying a configuration file, it will behave
    as if you pressed "Save As", instead.
* Save As
  * Prompts you to select where you would like to save the configuration file
    and saves it.
* Settings
  * Allows you to customize the application.
* Quit
  * Exits the application, discarding any unsaved changes to the configuration
    file.

### The Help Menu

In the "Help" menu, there are several options:

* Help
  * Opens this window.
* Report a Bug
  * Opens the bug reporting form in your browser.
  * Note that you will need to sign in to GitLab to use this form.
    * If you do not have a GitLab account, you can use other accounts to sign in,
      such as Google and GitHub.
* Request a Feature
  * Opens the feature request form in your browser.
  * Note that you will need to sign in to GitLab to use this form.
    * If you do not have a GitLab account, you can use other accounts to sign in,
      such as Google and GitHub.
* Legal Information
  * Opens a window containing details on the license this program has been
    distributed to you under.
* About Qt
  * Opens a window containing information about Qt, the graphical toolkit this
    application's GUI was built with.
* About Plex Permissions Fixer
  * Opens a window containing basic information about this application.

### CLI Help

For help with this application's command-line interface, please use the
help flags (`-h`, `--help`) when calling the program.
