## Application Overview

Plex Permissions Fixer is a tool built to solve permissions headaches for
Plex library folders on UNIX-based systems using a CLI or GUI.

Its usage is not just restricted to Plex - it can be used for other media
servers (such as Emby or Jellyfin) or for other applications entirely.
Plex was just the initial use case for it.

### What is Plex?

Plex (https://www.plex.tv/) is a media server that makes it easy to organize
and stream your media collection from anywhere with automatic transcoding
enabling it to be used from just about any device or connection.

### Bug Reports

(TODO)

### Feature Requests

(TODO)
