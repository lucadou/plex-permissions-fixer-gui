"""
This file is part of Plex Permissions Fixer, a simple application for setting
permissions on Plex library directories.
Copyright (C) 2022-2023 Luna Lucadou

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import json
import logging
import pathlib
from typing import List, Optional, cast, Tuple

import pydantic
from typing_extensions import NotRequired, TypedDict

import constants
#from app import constants

logger = logging.getLogger(__name__)


class DirEntry(TypedDict):
    dir: str
    perms: NotRequired[str]


class DirConf(TypedDict):
    default_perms: str
    dirs: List[DirEntry]


class PermsProgress(TypedDict):
    dir: DirEntry
    success: bool
    message: str


def get_fallback_conf(blank_dirs: int = 0) -> DirConf:
    """
    In the event that the configuration file is either not present or
    its contents are invalid, this method produces a base configuration
    which should be fallen back on.

    Because the GUI needs to insert some rows for the user to use at the start,
    the ``blank_dirs`` variable can be used to specify the number of empty rows
    to start the table off with.

    :param blank_dirs: number of blank DirEntry objects to include
    :return: a DirConf object with default permissions and zero directories
    """
    dirs: List[DirEntry] = []
    for _ in range(blank_dirs):
        empty_dir: DirEntry = {
            'dir': ''
        }
        dirs += [empty_dir]

    fallback_conf: DirConf = {
        'default_perms': constants.default_perms_fallback,
        'dirs': dirs
    }
    return fallback_conf


def read_conf(conf_path: str = constants.default_conf_location) -> Tuple[Optional[DirConf], Optional[str]]:
    """
    Reads the application configuration file and returns its configuration
    data, if the file is present and valid, with the second returned value
    being None.

    Returns None if configuration file is not present, is invalid, or incurs
    another error, the details of which will be present in the second returned
    value (str).

    :param conf_path: the configuration to file, if different from default
    :type conf_path: str
    :return: a DirConf representing the configuration data, if file was read,
        and a str, if there was an error
    """
    conf_data: Optional[DirConf] = None
    err_msg: Optional[str] = None
    try:
        # Import config data
        with open(conf_path, 'r') as conf_file:
            conf_data = DirConf(**json.load(conf_file))
        conf_file.close()
        # Validate config data
        conf_data: DirConf = cast(DirConf, pydantic.create_model_from_typeddict(DirConf)(**conf_data).dict())
        logger.info('Config file validated')
    except FileNotFoundError:
        logger.info('Config file not found at %s', conf_path)
        if conf_path == constants.default_conf_location:
            logger.info('Assuming first start')
        else:
            err_msg: str = constants.conf_state_not_found
        conf_data = None
    except PermissionError as err:
        logger.info('Config file access denied at %s', conf_path)
        logger.info('\t{0}'.format(err))
        err_msg: str = constants.conf_state_permissions
        conf_data = None
    except OSError as err:
        # OSError must go after PermissionError
        # Doesn't look as nice, but OSError is a superclass of PermissionError
        logger.info('Config file access incurred unknown error')
        logger.info('\t{0}'.format(err))
        err_msg: str = constants.conf_state_unknown
        conf_data = None
    except (json.decoder.JSONDecodeError,
            pydantic.error_wrappers.ValidationError) as err:
        logger.error('Config file invalid')
        logger.error('\t{0}'.format(err))
        err_msg: str = constants.conf_state_invalid
        conf_data = None

    # conf_data is either valid or None if it reaches this point
    return conf_data, err_msg


def write_conf_to_str(conf_data: DirConf) -> str:
    """
    Given a DirConf, returns it as a string

    :param conf_data: the configuration data to encode as str
    :return: the str-encoded dict
    """
    return json.dumps(conf_data)


def write_conf(conf_data: DirConf,
               conf_path: str = constants.default_conf_location) \
        -> Tuple[bool, Optional[str]]:
    """
    Validates the given configuration file and writes it to disk.

    If successful, returns [True, None].
    Else, returns [False, str], where str contains an error message.

    :param conf_data: the data to write to file
    :param conf_path: the path to write it to
    :return: a tuple containing a bool and a str
    """
    success: bool = False
    err_msg: Optional[str] = None
    try:
        conf_data: DirConf = cast(DirConf, pydantic.create_model_from_typeddict(DirConf)(**conf_data).dict())
        with open(conf_path, 'w') as conf_file:
            json.dump(conf_data, conf_file, indent=4)
        conf_file.close()
        success = True
    except pydantic.error_wrappers.ValidationError as err:
        logger.error('Config file invalid')
        logger.error('\t{0}'.format(err))
        err_msg: str = constants.conf_state_invalid
        success = False
    except PermissionError as err:
        logger.error('Config file access denied at {0}'.format(conf_path))
        logger.error('\t{0}'.format(err))
        err_msg: str = constants.conf_state_permissions
        success = False
    except OSError as err:
        # OSError must go after PermissionError
        # Doesn't look as nice, but OSError is a superclass of PermissionError
        logger.error('Config file access incurred unknown error')
        logger.error('\t{0}'.format(err))
        err_msg: str = constants.conf_state_unknown
        success = False

    return success, err_msg
