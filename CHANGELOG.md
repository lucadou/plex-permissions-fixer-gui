# Changelog
All notable changes to this project will be documented in this file, including:

- Changes requiring editing the config file
- Changes requiring recompiling translations
- Changes requiring newer versions of Python
- Changes requiring updating dependencies

## [0.3.0] - 2023-04-11
### Added
- Auto-generation of barebones config (#3)
- Settings menu (#4)
- Help menu (#6)

### Removed
- GUI class main method (#3)

## [0.2.0] - 2023-04-05
### Added
- GUI
- Underlying CLI application

## [0.1.0] - 2022-10-16
### Added
- Changelog
- License
- Readme

