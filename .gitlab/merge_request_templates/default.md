## Version: [version number]

### Notable Changes:

* Item1
* Item2
* Item3

### Explanations of Changes (if Needed):



### Merge Checklist:

- [ ] Changelog entry
- [ ] Changes tested (if needed)
- [ ] Configuration file updated (if needed)
- [ ] Dependencies updated (if needed)
- [ ] Documentation updated (if needed)
- [ ] Readme updated (if needed)
- [ ] Translations recompiled (if needed)
- [ ] Unnecessary git tags for previous commits in this MR removed
      (`git tag -d vX.Y.Z`)
- [ ] Git tag created for final commit prior to merge (`git tag vX.Y.Z`)
- [ ] Git tag pushed (`git push origin vX.Y.Z`)
