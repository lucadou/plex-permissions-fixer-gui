- [ ] I am reporting a bug with this application
- [ ] I have checked to make sure I am not filing a duplicate report

### Bug Description:

(Provide a brief description of what is going wrong)

### Expected Behavior:

(Provide a description of what you believe should be occurring)

### Actual Behavior:

(Provide a description of what is actually occurring)

### Additional Context:

(Add any additional background information, mockups, related issues, etc.)

### Steps to Reproduce:

1. x
2. y
3. z

### System Information:

* OS & Version:
* Output of `uname -mrsv`:
* Application Version:
* Application Source (apt repo, AppImage, source, etc.):
