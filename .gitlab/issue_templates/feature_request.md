### Feature Description:

(Provide a brief description of what you would like to be added to the application)

### Reasoning:

(Explain why you would like this feature to be added)

### Additional Context:

(Add any additional background information, mockups, related issues, etc.)

### Definition of Done:

- [ ] x
- [ ] y
- [ ] z
